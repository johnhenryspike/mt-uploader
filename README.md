# Multithreaded uploader php implementation
[Based on this node-js realisation](https://medium.com/@pilovm/multithreaded-file-uploading-with-javascript-dafabce34ccd).
This project have developed to show you how to create multithreaded file uploading in javascript and php.

## How to test it
You have to clone the repository and start the server
```
git clone https://gitlab.com/johnhenryspike/mt-uploader.git
```
```
docker-compose up -d
```
Then you can open `http://localhost:4000` and try to upload files. They will store in `files` folder.


