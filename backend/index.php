<?php
session_start();
/**
 * @return false|string
 * @throws Exception
 */
class Upload {

    public $id;
    private $name;
    private $contentSizes;
    private $chunks;
    private $size;
    private $chunksQuantity;
    private $chunksDone;
    private $target;

    public function __construct($name = '', $chunksQuantity = 0, $size = 0)
    {
        $this->name = $name;
        $this->chunksQuantity = $chunksQuantity;
        $this->size = $size;

        $this->contentSizes = [];
        $this->chunks = [];
        $this->chunksDone = 0;

        $this->id = md5(mktime());

        $this->target = './uploads/'.$this->id;

        $fout=fopen($this->target,"wb");
        fclose($fout);

        $_SESSION['upload_data'] =  $this;
    }

    public static function getInstance($id){
        if ($_SESSION['upload_data'] instanceof self) {
            $instance = $_SESSION['upload_data'];
            if ($instance->id == $id){
                return $instance;
            }
        }
        return false;
    }

    public function getChunkLength($id) {
        if (!$this->chunks[$id]) {
            return 0;
        }

        return $this->chunks[$id];
    }

    public function pushChunk($id, $contentLength) {
        $chunk_file = fopen($this->target.'_'.$id.'.tmp',"wb");
        $fin = fopen("php://input", "rb");
        if ($fin) {
            while (!feof($fin)) {
                $data=fread($fin, $contentLength);
                fwrite($chunk_file,$data);
            }
            fclose($fin);
        }

        fclose($chunk_file);

        $this->chunks[$id] = $contentLength;
        $this->chunksDone += 1;

        return true;
    }

    public function isCompleted() {
        return $this->chunksQuantity === $this->chunksDone;
    }

    public function save(){
        $file = fopen($this->target,"ab");
        for($i = 0; $i<count($this->chunks); $i++){
            $chunk = file_get_contents($this->target.'_'.$i.'.tmp');
            fwrite($file,$chunk);
            unlink($this->target.'_'.$i.'.tmp');
        }
        fclose($file);
        rename('./uploads/'.$this->id, './uploads/'.$this->name);
        $this->destroy();
    }

    private function destroy(){
        if ($_SESSION['upload_data'] instanceof self) {
            unset($_SESSION['upload_data']);
            return true;
        }
        return false;
    }
}

/**
 * @param array $headers
 * @return array|false
 * @throws Exception
 */
function getHeaders($headers = []){
    $arHeaders = getallheaders();
    foreach ($headers as $header){
        if (!key_exists($header, $arHeaders)) {
            throw new Exception("Request has no ".$header." header", 500);
        }
    }
    return $arHeaders;
}

/**
 * @return false|string
 * @throws Exception
 */

function initUploading() {
    $arHeaders = getHeaders(["X-Content-Name", "X-Content-Length", "X-Chunks-Quantity"]);

    $name = $arHeaders["X-Content-Name"];
    $size = (int)$arHeaders["X-Content-Length"];
    $chunksQuantity = (int)$arHeaders["X-Chunks-Quantity"];

    $ifile = new Upload($name, $chunksQuantity, $size);
    $responce = [
        'status' => 200,
        'fileId' => $ifile->id
    ];
    return json_encode($responce);
}

/**
 * @return false|string
 * @throws Exception
 */
function loadingByChunks() {
    $arHeaders = getHeaders(["X-Content-Id", "X-Chunk-Id"]);

    $fileId = $arHeaders["X-Content-Id"];
    $chunkId = $arHeaders["X-Chunk-Id"];
    $chunkSize = (int)$arHeaders["Content-Length"];

    $file = Upload::getInstance($fileId);
    if (!$file) {
        throw new Exception("Can't get instance of Upload.", 500);
    }

    $chunkComplete = $file->pushChunk($chunkId, $chunkSize);

    if (!$chunkComplete) {
        throw new Exception("Chunk uploading was not completed", 500);
    }

    $size = $file->getChunkLength($chunkId);
    if ($file->isCompleted()) {
        $file->save();
    }

    $responce = [
        'status' => 200,
        'size' => $size
    ];
    return json_encode($responce);
}

header("Content-Type: application/json");
if (key_exists('init',$_REQUEST)){
    echo initUploading();
}else {
    echo loadingByChunks();
}